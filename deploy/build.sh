#!/bin/bash

#used in pipelines deployment for creating zip file with production code

if [ -z "$1" ]; then
  echo "Parametar file is mandatory"
  exit 1
else
  ZIP_FILE=$1
fi

echo "making $ZIP_FILE"

zip -r -X $ZIP_FILE app bootstrap config database deploy_scripts public resources routes storage \
 artisan composer.json composer.lock appspec.yml .env
