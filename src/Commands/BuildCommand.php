<?php


namespace Superius\Deploy\Commands;

use Illuminate\Console\Command;

/**
 * Class BuildCommand
 * @package Superius\Deploy\Commands
 * prepare files that will be used for build and deploy with AWS Code Deploy
 * used in pipelines
 */
class BuildCommand extends Command
{

    protected $signature = 'omni:build';

    protected $description = 'Prepare and zip deployment';

    public function __construct()
    {
        parent::__construct();
    }

    final public function handle(): void
    {
        $deployScriptsDir = 'deploy_scripts';

        //defined in bitbucket: Repository settings > Deployments
        $deployDir = getenv('APP_DEPLOY_DIR');
        if (!$deployDir) {
            throw new \Exception('APP_DEPLOY_DIR is not defined!');
        }

        if (!file_exists($deployScriptsDir) && !mkdir($deployScriptsDir) && !is_dir($deployScriptsDir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $deployScriptsDir));
        }

        foreach (glob('vendor/superius/deploy/deploy/*') as $file) {
            $fileContent = file_get_contents($file);
            $fileContent = str_replace('##PATH##', $deployDir, $fileContent);
            $fileName = basename($file);

            if($fileName === 'appspec.yml'){//code deploy conf file
                //put to root
                file_put_contents($fileName, $fileContent);
            }
            else{
                //put to deploy folder
                file_put_contents($deployScriptsDir.'/'.$fileName, $fileContent);
            }
        }
    }
}
