<?php

namespace Superius\Deploy\Commands;

use Exception;
use Dotenv\Dotenv;
use Illuminate\Console\Command;

/**
 * Class CheckEnvCommand
 * @package Superius\Deploy\Commands
 * checking generated .env file for errors or undefined constants
 * used as stand alone command with "sniff" command
 */
class CheckEnvCommand extends Command
{
    protected $signature = 'omni:checkenv {--file=*}';
    protected $description = 'Cheking env files';

    public function __construct()
    {
        parent::__construct();
    }

    final public function handle(): void
    {
        $files = $this->option('file');

        foreach ($files as $file) {
            if (!file_exists($file)) {
                throw new \RuntimeException(sprintf('File "%s" is missing or invalid', $file));
            }
            $source = file_get_contents($file);
            if (strpos($source, "\r\n") !== false) {////ako je windows end of line baciti će exception
                throw new Exception(' Windows line endings in file :'.$file);
            }
            Dotenv::parse($source);///ako env nije validan parser će baciti exception
        }
    }
}
