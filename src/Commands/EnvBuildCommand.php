<?php

namespace Superius\Deploy\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;

/**
 * Class EnvBuildCommand
 * @package Superius\Deploy\Commands
 * building production env file
 * used in pipelines
 */
class EnvBuildCommand extends Command
{

    protected $signature = 'omni:env-build';

    protected $description = 'Build .env file for production deploy';

    private $sourcePath = '.env.build';

    public function __construct()
    {
        parent::__construct();
    }

    final public function handle(): void
    {
        //do not replace
        if(file_exists('.env')){
            throw new \RuntimeException('.env already exists');
        }

        //get template
        if (!is_readable($this->sourcePath)) {
            throw new \RuntimeException(sprintf('%s file is not readable', $this->sourcePath));
        }

        $lines = explode("\n", file_get_contents($this->sourcePath));
        foreach ($lines as $key => $line) {

            if (!$line || strpos(trim($line), '#') === 0) {
                continue;
            }

            list($name, $value) = explode('=', $line, 2);
            $name = trim($name);
            $value = trim($value);

            if(Str::startsWith($value, '## ') && Str::endsWith($value, ' ##')){
                $varName = str_replace(['## ', ' ##'], '', $value);
                if(getenv($varName)){
                    $lines[$key] = $name.'='.getenv($varName);
                }else{
                    throw new \RuntimeException(sprintf('%s Variable is missing', $varName));
                }
            }
        }

        file_put_contents('.env', implode("\n", $lines));
    }
}
