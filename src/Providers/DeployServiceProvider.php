<?php

namespace Superius\Deploy\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;
use Superius\Deploy\Commands\BuildCommand;
use Superius\Deploy\Commands\CheckEnvCommand;
use Superius\Deploy\Commands\EnvBuildCommand;

class DeployServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        //scripts that prepares build
        if ($this->app->runningInConsole()) {
            $this->commands([
                BuildCommand::class,
                EnvBuildCommand::class,
                CheckEnvCommand::class,
            ]);
        }
    }

    public function boot(): void
    {
        if ($this->app->environment('local')) {
            $source = base_path('vendor/superius/deploy/bitbucket-pipelines.yml');
            $destination = base_path('bitbucket-pipelines.yml');

            File::copy($source, $destination);
        }
    }
}
